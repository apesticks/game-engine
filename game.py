import pygame, registry
from pygame import *

class Game(object):
    def __init__(self):
        registry.running = True
        registry.frame = 0
        self.infinite = False

    def tick(self):
        registry.events = pygame.event.get()
        registry.frame = registry.frame + 1

    def loop(self, frames=None):
        num = 0

        while registry.running == True:
            self.tick()

            if frames != None:
                # frames is 0-based indexing
                if num == frames - 1:
                    break

                num += 1


    def add_event(self, event_type, key):
        type_name = getattr(pygame.locals, event_type)
        key_code = getattr(pygame.locals, key)

        event = pygame.event.Event(type_name, {"key":key_code})
        pygame.event.post(event)

if __name__ == "__main__":
    game = Game()
    game.loop()
