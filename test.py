import pygame, pytest, pdb
from pygame.locals import *

from game import Game
import registry

def test_pygame_basic_env():
    assert registry.screen_size == (800, 600)
    assert isinstance(registry.screen, pygame.Surface)
    assert registry.screen.get_size() == registry.screen_size
    assert registry.running == False
    assert registry.frame == None

def test_frame_increment():
    game = Game()
    assert registry.running == True

    assert registry.frame == 0
    game.tick()
    assert registry.frame == 1

def test_game_loop():
    game = Game()

    assert registry.frame == 0
    game.loop(3)
    assert registry.frame == 3
    game.loop(3)
    assert registry.frame == 6

def test_event_queue():
    game = Game()

    assert registry.events == []

    game.add_event('KEYDOWN', 'K_UP')
    game.loop(1)
    assert registry.events != []
    assert registry.events[0].key == K_UP

    game.loop(1)
    assert registry.events == []
